from django.shortcuts import render, render_to_response
from django.template import RequestContext

from models import Portfolio


def home(request):
    return render_to_response(
        'adaptiware/home.html',
        context_instance=RequestContext(request)
    )

def contacts(request):
    return render_to_response(
        'adaptiware/contacts.html',
        context_instance=RequestContext(request)
    )

def portfolio(request):
    q = Portfolio.objects.all()
    return render(request, 'adaptiware/portfolio.html',
            { 'portfolio': q }
    )
