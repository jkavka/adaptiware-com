from django.contrib import admin

from models import Portfolio

class PortfolioAdm(admin.ModelAdmin):
    """
    Portfolio administration.
    """
    # {{{
    list_display = ('name', 'client', 'launched', 'url')
    actions = ['bulk_delete']

    def bulk_delete(self, request, queryset):
        for obj in queryset:
            self.delete_model(request, obj)

    bulk_delete.short_description = "Delete selected {0}".format(
            Portfolio._meta.verbose_name_plural)

    def get_actions(self, request):
        actions = super(PortfolioAdm, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions
    # }}}

admin.site.register(Portfolio, PortfolioAdm)

# vim: nowrap tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker :
