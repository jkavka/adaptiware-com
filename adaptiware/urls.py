from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from django.conf.urls.static import static

admin.autodiscover()

media = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = patterns('',
   url(r'^$', 'adaptiware.views.home', name='home'),
   url(r'^contacts/', 'adaptiware.views.contacts', name='contacts'),
   url(r'^portfolio/', 'adaptiware.views.portfolio', name='portfolio'),
   url(r'^admin/', include(admin.site.urls)),
) + media
