from django.db import models

class Portfolio(models.Model):
    """
    Model for portfolio
    """
    # {{{
    name = models.CharField('Name', max_length=100, blank=False, null=True)
    client = models.CharField('Client', max_length=200, blank=False, null=True)
    image = models.ImageField('Image', upload_to='portfolio', blank=False, null=True)
    launched = models.DateField('Launched', blank=False, null=True)
    url = models.URLField('URL', blank=True, null=True)

    class Meta:
        verbose_name = 'portfolio item'
        verbose_name_plural = 'portfolio items'
        ordering = ['name']

    def __unicode__(self):
        return "{0}, {1}".format(self.name, self.client)

    def delete(self, *args, **kwargs):
        """
        Function for deleting portfolio from db. It also deletes 
        the image file from the storage.
        """
        
        # Deletes the image file from the storage
        self.image.delete()

        # Delete row from database
        super(Portfolio, self).delete(*args, **kwargs)

    # }}}

# vim: nowrap tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker :
